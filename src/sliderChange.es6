import int from 'int'
import setDrop from './src/setDrop.es6'
import displayThumb from './src/displayThumb.es6'
/**
 * * This is a crucial part of the update loop!
 * calls setDrop() and calls displayThumb()
 */
class sliderChange {
  constructor () {
    this.repW = int(this.repWSl.value)
    this.repH = int(this.repHSl.value)
    setDrop()
    displayThumb()
    }
}