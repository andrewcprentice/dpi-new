import int from 'int'
import BitmapData from '../node_modules/openfl/lib/openfl/display/BitmapData'
import Matrix from '../node_modules/openfl/lib/openfl/geom/Matrix'
import Point from '../node_modules/openfl/lib/openfl/geom/Point'
import updateRepMsg from './updateRepMsg.es6'
class stepAndRepeatDisplay {
  constructor () {
    this.previewFactor = 1
    this.previewImg = new BitmapData()
    this.matrix = new Matrix()
    this.previewImg.width = this.repeatContainer.width
    this.previewImg.height = this.repeatContainer.height
    this.dropWFactor = 1
    this.dropHFactor = 1
    // think about zoomFactor for showing more repeats
    switch (this.drop.selectedIndex.toString()) {
      case '0':
        break
      case '1':
        this.dropWFactor = this.dropXY.selectedIndex + 2
        break
      case '2':
        this.dropHFactor = this.dropXY.selectedIndex + 2
        break
    }
    this.wFactor = int(this.repW / this.repeatContainer.width) + 1
    this.hFactor = int(this.repH / this.repeatContainer.height) + 1
    if (this.hFactor >= this.wFactor) {
      this.previewFactor = this.hFactor
    } else {
      this.previewFactor = this.wFactor
    }
    this.matrix.scale(1 / this.previewFactor, 1 / this.previewFactor)
    this.previewImgW = int(this.tempPreviewBMD.width / this.previewFactor) * this.dropWFactor
    this.previewImgH = int(this.tempPreviewBMD.height / this.previewFactor) * this.dropHFactor
    this.compImgW = this.previewImgW / this.dropWFactor
    this.compImgH = this.previewImgH / this.dropHFactor
    this.previewBitmapData = new BitmapData(this.previewImgW, this.previewImgH, true, 0x00FFFFFF)
    this.previewBitmapData.draw(this.tempPreviewBMD, this.matrix)
    switch (this.drop.selectedIndex.toString()) {
      case '0':
        break
      case '1':
        this.dropSectionSize = this.previewImgH - (this.previewImgH / (this.dropXY.selectedIndex + 2))
        for (let dropWRep = 0; dropWRep <= this.dropXY.selectedIndex; dropWRep++) {
          this.fromX = dropWRep * this.compImgW
          this.toX = (dropWRep + 1) * this.compImgW
          this.previewBitmapData.copyPixels(this.previewBitmapData, new Rectangle(this.fromX, 0, this.compImgW, this.dropSectionSize), new Point(this.toX, this.compImgH - this.dropSectionSize))
          this.previewBitmapData.copyPixels(this.previewBitmapData, updateRepMsg(new Rectangle(this.fromX, this.dropSectionSize, this.compImgW, this.compImgH - this.dropSectionSize), new Point(this.toX, 0)))
          break
        }
        break
      case '2':
        this.dropSectionSize = this.previewImgW - (this.previewImgW / (this.dropXY.selectedIndex + 2))
        for (let dropHRep = 0; dropHRep <= this.dropXY.selectedIndex; dropHRep++) {
          this.fromY = dropHRep * this.compImgH
          this.toY = (dropHRep + 1) * this.compImgH
          this.previewBitmapData.copyPixels(this.previewBitmapData, new Rectangle(0, this.fromY, this.dropSectionSize, this.compImgH), new Point(this.compImgW - this.dropSectionSize, this.toY))
          this.previewBitmapData.copyPixels(this.previewBitmapData, new Rectangle(this.dropSectionSize, this.fromY, this.compImgW - this.dropSectionSize, this.compImgH), new Point(0, this.toY))
        }
        break
    }
    this.previewImg.source = this.previewBitmapData
    console.log('stepAndRepeatDisplay')
  }
}
