import int from 'int'
import recenterDesignWindow from './recenterDesignWindow.es6'
/**
 * calls recenterDesignWindow()
 */
class resizeDesignWindow {
  constructor () {
    this.designView = document.getElementById('designView')
    this.lastDesignViewWidth = this.designView.width
    this.lastDesignViewHeight = this.designView.height
    this.designViewFactor = 100 / this.designZoom.value
    this.designView.width = int(this.designW / this.designViewFactor)
    this.designView.height = int(this.designH / this.designViewFactor)
    console.log('resizeDesignWindow')
    recenterDesignWindow()
  }
}
