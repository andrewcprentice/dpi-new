/**
 * Update repeat message
 * @param {String} msg Repeat message text. '-' for no message
 * @param {String} type Info or Error
 */
class updateRepMsg {
  constructor (msg, type) {
    this.repMsg = document.getElementById('repMsg')
    console.log('updateRepMsg')
    switch (type) {
      case 'Info':
        if (msg !== ' - ') {
          this.repMsg.innerText = msg
        }
        break
      case 'Error':
        this.repMsg.innerText = msg
        break
    }
  }
}
