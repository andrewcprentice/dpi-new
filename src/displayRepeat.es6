import BitmapData from '../node_modules/openfl/lib/openfl/display/BitmapData'
import Point from '../node_modules/openfl/lib/openfl/geom/Point'
import Rectangle from '../node_modules/openfl/lib/openfl/geom/Rectangle'

import calculateRepeat from './calculateRepeat.es6'
import stepAndRepeatDisplay from './stepAndRepeatDisplay.es6'

/**
 * calls calculateRepeat()
 * calls stepAndRepeatDisplay
 */
class displayRepeat {
  constructor () {
    this.tempPreviewBMD = new BitmapData(this.repW, this.repH, true, 0xFFFFFFFF)
    this.tempPreviewBMD.copyPixels(this.designBitMapData, new Rectangle(this.xRepStart, this.yRepStart, this.repW, this.repH), new Point(0, 0))
    this.tempPreviewBMD.lock()
    console.log('displayRepeat')
    calculateRepeat() // Setup repeat with processed sections
    this.tempPreviewBMD.unlock()
    stepAndRepeatDisplay() // Step & Repeat 'the repeat' for Display
  }
}
