import int from 'int'

import resizeDesignWindow from './resizeDesignWindow.es6'

/**
 * calls resizeDesignWindow()
  */
class defaultDesignWindow {
  constructor () {
    this.designZoom = document.getElementById('designZoom')
    this.designView = document.createElement('img')
    this.designView.id = 'designView'
    document.body.appendChild(this.designView)
    this.designZoom.maximum = 100
    if ((this.designH >= this.maxDesignView) || (this.designW >= this.maxDesignView)) {
      if (this.designH >= this.designW) {
        this.designZoom.maximum = int(100 / (this.designH / this.maxDesignView))
      } else {
        this.designZoom.maximum = int(100 / (this.designW / this.maxDesignView))
      }
    }
    if ((this.designH / this.designWindow.height) >= (this.designW / this.designWindow.width)) {
      this.designViewFactor = this.designH / this.designWindow.height
    } else {
      this.designViewFactor = this.designW / this.designWindow.width
    }
    this.designView.width = int(this.designW / this.designViewFactor)
    this.designView.height = int(this.designH / this.designViewFactor)
    this.designZoom.value = 100 / this.designViewFactor
    this.designZoom.minimum = 100 / this.designViewFactor
    console.log('defaultDesignWindow')
    resizeDesignWindow()
  }
}
