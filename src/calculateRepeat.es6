import BitmapData from '../node_modules/openfl/lib/openfl/display/BitmapData'
import Rectangle from '../node_modules/openfl/lib/openfl/geom/Matrix'
import Point from '../node_modules/openfl/lib/openfl/geom/Point'

class calculateRepeat {
  constructor () {
    this.tempPreviewBMD = new BitmapData(this.repW, this.repH, true, 0xFFFFFFFF)
    this.tempPreviewBMD.copyPixels(this.designBitMapData, new Rectangle(this.xRepStart, this.yRepStart, this.repW, this.repH), new Point(0, 0))
    this.tempPreviewBMD.lock()
    this.advanced = document.getElementById('advanced')
    console.log('calculateRepeat')
    for (let index = 1; index <= 8; index++) {
      this.fobj = this.advanced.childNodes[index]
      this.sectID = this.fobj.id
      switch (this.sectID) {
        case 'TL': // top left
          if (this.TLInc.selected === true) {
            this.sectBMD = new BitmapData(this.xRepStart, this.yRepStart, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle(0, 0, this.xRepStart, this.yRepStart), new Point(0, 0))
            this.xOffset = (this.repW - this.xRepStart) + this.xDrop
            this.yOffset = (this.repH - this.yRepStart) + this.yDrop
            for (let xTL = 0; xTL < this.xRepStart; xTL++) {
              for (let yTL = 0; yTL < this.yRepStart; yTL++) {
                this.srcPixClr = this.sectBMD.getPixel(xTL, yTL)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.TLBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xTL) % this.repW), ((this.yOffset + yTL) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xTL) % this.repW), ((this.yOffset + yTL) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xTL) % this.repW), ((this.yOffset + yTL) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'TC': // top center
          if (this.TCInc.selected === true) {
            this.sectBMD = new BitmapData(this.repW, this.yRepStart, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle(this.xRepStart, 0, this.repW, this.yRepStart), new Point(0, 0))
            this.xOffset = this.xDrop
            this.yOffset = (this.repH - this.yRepStart)
            for (let xTM = 0; xTM < this.repW; xTM++) {
              for (let yTM = 0; yTM < this.yRepStart; yTM++) {
                this.srcPixClr = this.sectBMD.getPixel(xTM, yTM)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.TCBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xTM) % this.repW), ((this.yOffset + yTM) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xTM) % this.repW), ((this.yOffset + yTM) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xTM) % this.repW), ((this.yOffset + yTM) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'TR': // top right
          if (this.TRInc.selected === true) {
            this.sectBMD = new BitmapData(this.xRepRemainder, this.yRepStart, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle((this.designW - this.xRepRemainder), 0, this.xRepRemainder, this.yRepStart), new Point(0, 0))
            this.xOffset = this.xDrop
            this.yOffset = (this.repH - this.yRepStart) + this.yDrop
            for (let xTR = 0; xTR < this.xRepRemainder; xTR++) {
              for (let yTR = 0; yTR < this.yRepStart; yTR++) {
                this.srcPixClr = this.sectBMD.getPixel(xTR, yTR)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.TRBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xTR) % this.repW), ((this.yOffset + yTR) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xTR) % this.repW), ((this.yOffset + yTR) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xTR) % this.repW), ((this.yOffset + yTR) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'ML': // middle left
          if (this.MLInc.selected === true) {
            this.sectBMD = new BitmapData(this.xRepStart, this.repH, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle(0, this.yRepStart, this.xRepStart, this.repH), new Point(0, 0))
            this.xOffset = this.repW - this.xRepStart
            this.yOffset = this.yDrop
            for (let xML = 0; xML < this.xRepStart; xML++) {
              for (let yML = 0; yML < this.repH; yML++) {
                this.srcPixClr = this.sectBMD.getPixel(xML, yML)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.MLBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xML) % this.repW), ((this.yOffset + yML) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xML) % this.repW), ((this.yOffset + yML) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xML) % this.repW), ((this.yOffset + yML) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'MR': // middle right
          if (this.MRInc.selected === true) {
            this.sectBMD = new BitmapData(this.xRepRemainder, this.repH, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle((this.designW - this.xRepRemainder), this.yRepStart, this.xRepRemainder, this.repH), new Point(0, 0))
            this.xOffset = 0
            this.yOffset = this.repH - this.yDrop
            for (let xMR = 0; xMR < this.xRepRemainder; xMR++) {
              for (let yMR = 0; yMR < this.repH; yMR++) {
                this.srcPixClr = this.sectBMD.getPixel(xMR, yMR)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.MRBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xMR) % this.repW), ((this.yOffset + yMR) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xMR) % this.repW), ((this.yOffset + yMR) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xMR) % this.repW), ((this.yOffset + yMR) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'BL': // bottom left
          if (this.BLInc.selected === true) {
            this.sectBMD = new BitmapData(this.xRepStart, this.yRepRemainder, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle(0, this.designH - this.yRepRemainder, this.xRepStart, this.yRepRemainder), new Point(0, 0))
            this.xOffset = (this.repW - this.xRepStart) + this.xDrop
            this.yOffset = this.yDrop
            for (let xBL = 0; xBL < this.xRepStart; xBL++) {
              for (let yBL = 0; yBL < this.yRepRemainder; yBL++) {
                this.srcPixClr = this.sectBMD.getPixel(xBL, yBL)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.BLBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xBL) % this.repW), ((this.yOffset + yBL) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xBL) % this.repW), ((this.yOffset + yBL) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xBL) % this.repW), ((this.yOffset + yBL) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'BC': // bottom center
          if (this.BCInc.selected === true) {
            this.sectBMD = new BitmapData(this.repW, this.yRepRemainder, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle(this.xRepStart, this.designH - this.yRepRemainder, this.repW, this.yRepRemainder), new Point(0, 0))
            this.xOffset = this.repW - this.xDrop
            this.yOffset = 0
            for (let xBC = 0; xBC < this.repW; xBC++) {
              for (let yBC = 0; yBC < this.yRepRemainder; yBC++) {
                this.srcPixClr = this.sectBMD.getPixel(xBC, yBC)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.BCBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xBC) % this.repW), ((this.yOffset + yBC) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xBC) % this.repW), ((this.yOffset + yBC) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xBC) % this.repW), ((this.yOffset + yBC) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
        case 'BR': // bottom right
          if (this.BRInc.selected === true) {
            this.sectBMD = new BitmapData(this.xRepRemainder, this.yRepRemainder, true, 0xFFFFFFFF)
            this.sectBMD.copyPixels(this.designBitMapData, new Rectangle(this.designW - this.xRepRemainder, this.designH - this.yRepRemainder, this.xRepRemainder, this.yRepRemainder), new Point(0, 0))
            this.xOffset = this.xDrop
            this.yOffset = this.yDrop
            for (let xBR = 0; xBR < this.xRepRemainder; xBR++) {
              for (let yBR = 0; yBR < this.yRepRemainder; yBR++) {
                this.srcPixClr = this.sectBMD.getPixel(xBR, yBR)
                if (this.srcPixClr !== this.bkgPixelColor) {
                  if (this.BRBO.selected === true) {
                    this.destPixClr = this.tempPreviewBMD.getPixel(((this.xOffset + xBR) % this.repW), ((this.yOffset + yBR) % this.repH))
                    if (this.destPixClr === this.bkgPixelColor) {
                      this.tempPreviewBMD.setPixel(((this.xOffset + xBR) % this.repW), ((this.yOffset + yBR) % this.repH), this.srcPixClr)
                    }
                  } else {
                    this.tempPreviewBMD.setPixel(((this.xOffset + xBR) % this.repW), ((this.yOffset + yBR) % this.repH), this.srcPixClr)
                  }
                }
              }
            }
          }
          break
      }
    }
  }
}
