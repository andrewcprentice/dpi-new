import updateRepMsg from './updateRepMsg.es6'

class setDrop {
  constructor() {
    this.drop = document.getElementById('drop')
    this.dropXY = document.getElementById('dropXY')
    this.printDrop = document.getElementsByClassName('printDrop')
    console.log('setDrop')
    switch (this.drop.selectedIndex.toString()) {
      case '0':
        this.dropValue = 0
        this.dropXY.enabled = false
        updateRepMsg(' - ', 'Info')
        this.xDrop = 0
        this.yDrop = 0
        this.printDrop.selectedIndex = 0
        break
      case '1':
        this.dropXY.enabled = true
        switch (this.dropXY.selectedIndex.toString()) {
          case '0':
            this.dropValue = this.repHSl.value / 2
            if ((this.repH % 2) > 0) {
              updateRepMsg("Drop doesn't divide equally !", 'Error')
            } else {
              updateRepMsg(' - ', 'Info')
            }
            this.printDrop.selectedIndex = 1
            break
          case '1':
            this.dropValue = this.repHSl.value / 3
            if ((this.repHSl.value % 3) > 0) {
              updateRepMsg("Drop doesn't divide equally !", 'Error')
            } else {
              updateRepMsg(' - ', 'Info')
            }
            this.printDrop.selectedIndex = 2
            break
          case '2':
            this.dropValue = this.repHSl.value / 4
            if ((this.repHSl.value % 4) > 0) {
              updateRepMsg("Drop doesn't divide equally !", 'Error')
            } else {
              updateRepMsg(' - ', 'Info')
            }
            this.printDrop.selectedIndex = 3
            break
        }
        this.yDrop = this.dropValue
        this.xDrop = 0
        break
      case '2':
        this.dropXY.enabled = true
        switch (this.dropXY.selectedIndex.toString()) {
          case '0':
            this.dropValue = this.repWSl.value / 2
            if ((this.repWSl.value % 2) > 0) {
              updateRepMsg("Drop doesn't divide equally !", 'Error')
            } else {
              updateRepMsg(' - ', 'Info')
            }
            this.printDrop.selectedIndex = 4
            break
          case '1':
            this.dropValue = this.repWSl.value / 3
            if ((this.repWSl.value % 3) > 0) {
              updateRepMsg("Drop doesn't divide equally !", 'Error')
            } else {
              updateRepMsg(' - ', 'Info')
            }
            this.printDrop.selectedIndex = 5
            break
          case '2':
            this.dropValue = this.repWSl.value / 4
            if ((this.repWSl.value % 4) > 0) {
              updateRepMsg("Drop doesn't divide equally !", 'Error')
            } else {
              updateRepMsg(' - ', 'Info')
            }
            this.printDrop.selectedIndex = 6
            break
        }
        this.xDrop = this.dropValue
        this.yDrop = 0
        break
    }
  }
}