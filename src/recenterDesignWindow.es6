/**
 * calls checkDesignPosition()
 */
class recenterDesignWindow {
  constructor () {
    // This already called during init()
    // this.designView = BitmapData.loadFromFile('assets/floral1.png')
    // this.designView.id = 'designView'
    if (this.designZoom.value === this.designZoom.minimum) {
      this.designView.x = ((this.designWindow.width / 2) - (this.designView.width / 2))
      this.designView.y = ((this.designWindow.height / 2) - (this.designView.height / 2))
    } else {
      if (this.designView.x < 0) {
        this.zoomXDif = (this.designWindow.width / 2) + (this.designView.x * -1)
      } else {
        this.zoomXDif = (this.designWindow.width / 2) - this.designView.x
      }
      if (this.designView.y < 0) {
        this.zoomYDif = (this.designWindow.height / 2) + (this.designView.y * -1)
      } else {
        this.zoomYDif = (this.designWindow.height / 2) - this.designView.y
      }
      this.xOffsetMult = this.lastDesignViewWidth / this.zoomXDif
      this.yOffsetMult = this.lastDesignViewHeight / this.zoomYDif
      this.designView.x = this.designView.x - ((this.designView.width - this.lastDesignViewWidth) / this.xOffsetMult)
      this.designView.y = this.designView.y - ((this.designView.height - this.lastDesignViewHeight) / this.yOffsetMult)
      console.log('recenterDesignWindow')
      checkDesignPosition()
    }
  }
}