export default AppSprite

import int from 'int'

import Bitmap from '../node_modules/openfl/lib/openfl/display/Bitmap'
import BitmapData from '../node_modules/openfl/lib/openfl/display/BitmapData'
import DOMSprite from '../node_modules/openfl/lib/openfl/display/DOMSprite'
import Rectangle from '../node_modules/openfl/lib/openfl/geom/Rectangle'
import ColorTransform from '../node_modules/openfl/lib/openfl/geom/ColorTransform'
import Matrix from '../node_modules/openfl/lib/openfl/geom/Matrix'
import Event from '../node_modules/openfl/lib/openfl/events/Event'
import Sprite from '../node_modules/openfl/lib/openfl/display/Sprite'

import defaultDesignWindow from './defaultDesignWindow.es6'
import displayRepeat from './displayRepeat.es6'
// import displayThumb from './displayThumb.es6'
import setDrop from './setDrop.es6'
// import sliderChange from './sliderChange.es6'
import updateRepMsg from './updateRepMsg.es6'

class AppSprite extends Sprite {
  constructor () {
    super()
    /**
     * calls loadDesignComplete()
     */
    function init () {
      console.log('init')
      loadDesignComplete()
    }
    /**
     * calls init()
     * calls plusMinus(new Event())
     * calls designWindowChanged()
     * calls repeatContainerWindowChanged()
     * calls dropChanged()
     * calls updateRepMsg(' - ', 'Info')
     * calls displayThumb()
     */
    class initRepeatImageVars {
      constructor () {
        this.tempPreviewBMD = new BitmapData()
        this.designBitMapData = new BitmapData()
        this.thumbBMD = new BitmapData()
        this.sectBMD = new BitmapData()
        this.maxDesignView = 8000
        this.hiloLightValue = -30
        this.previewImgW = 8000
        this.previewImgH = 8000
        this.compImgW = 8000
        this.compImgH = 8000
        this.designH = 8000
        this.designW = 8000
        this.thumbWidth = 80
        this.thumbHeight = 80
        this.TCInc = document.getElementById('TCInc')
        this.TLInc = document.getElementById('TLInc')
        this.TLBO = document.getElementById('TLBO')
        this.TCBO = document.getElementById('TCBO')
        this.TRBO = document.getElementById('TRBO')
        this.TRInc = document.getElementById('TRInc')
        this.MLInc = document.getElementById('MLInc')
        this.MLBO = document.getElementById('MLBO')
        this.MRInc = document.getElementById('MRInc')
        this.MRBO = document.getElementById('MRBO')
        this.BLInc = document.getElementById('BLInc')
        this.BLBO = document.getElementById('BLBO')
        this.BCInc = document.getElementById('BCInc')
        this.BCBO = document.getElementById('BCBO')
        this.BRInc = document.getElementById('BRInc')
        this.BRBO = document.getElementById('BRBO')
        this.drop = document.getElementById('drop')
        this.dropXY = document.getElementById('dropXY')
        this.repWSl = document.getElementById('repWSl')
        this.repHSl = document.getElementById('repHSl')
        this.repZSl = document.getElementById('repZSl')
        this.designWindow = document.getElementById('designWindow')
        this.repeatContainer = document.getElementsByClassName('repeatContainer')
        this.designZoom = document.getElementById('designZoom')
        this.designWindowSprite = new DOMSprite(this.designWindow)
        this.repeatContainerSprite = new DOMSprite(this.repeatContainer)
        this.designView = document.getElementById('designView')
        this.designViewSprite = new DOMSprite(this.designView)
        this.previewImg = document.getElementById('previewImg')
        this.prSprite = new DOMSprite(this.previewImg)
        this.designViewTools = document.getElementsByClassName('designViewTools')
        this.dVTSprite = new DOMSprite(this.designViewTools)
        this.dZSprite = new DOMSprite(this.designZoom)
        this.thumbNail = new Bitmap(this.thumbBMD)
        this.repWSl.maximum = this.designW - 30
        this.repWSl.minimum = int(this.designW / 2) + 1
        this.repWSl.value = this.repWSl.maximum
        this.repW = this.repWSl.maximum
        this.repHSl.maximum = this.designH - 30
        this.repHSl.minimum = int(this.designH / 2) + 1
        this.repHSl.value = this.repHSl.maximum
        this.repH = this.repHSl.maximum
        // * HARDCODED BITMAP
        BitmapData.loadFromFile('dist/assets/floral1.png').onComplete(bitmapData => {
          this.bitmap = new Bitmap(bitmapData)
          this.stage.get('#designWindow')
          this.addChild(this.bitmap)
          this.addChild(this.thumbNail)
          this.addChild(this.designWindowSprite)
          this.addChild(this.prSprite)
          this.addChild(this.repeatContainerSprite)
          this.addChild(this.designViewSprite)
          this.addChild(this.dVTSprite)
          this.addChild(this.dZSprite)
          init()
          console.log('initRepeatImageVars')
          plusMinus(new Event())
          designWindowChanged()
          repeatContainerWindowChanged()
        }).onError(e => console.error(e))
        // These method calls are outside of BitmapData.loadFromFile()
        dropChanged()
        updateRepMsg(' - ', 'Info')
        displayThumb()
      }
    }
    /**
     * calls defaultDesignWindow(), initRepeatImageVars()
     */
    class loadDesignComplete {
      constructor () {
        this.designXY = document.getElementById('designXY')
        this.repDesignXY = document.getElementById('repDesignXY')
        this.imageRes = 150
        this.resXY = document.getElementById('resXY')
        this.printUnit = document.getElementById('printUnit')
        this.designView = document.createElement('img')
        this.previewImg = document.createElement('img')
        this.designView.id = 'designView'
        this.previewImg.id = 'previewImg'
        this.designXY.innerText = `${this.designW.toString()}x X ${this.designH.toString()}y`
        this.repDesignXY.innerText = this.designXY.innerText
        this.resXY.innerText = `Resolution:${this.imageRes}`
        if (this.printUnit.selectedIndex === 0) {
          this.resXY.innerText = `${this.resXY.innerText} DPI`
        } else {
          this.resXY.innerText = `${this.resXY.innerText} DPcm`
        }
        this.designWindow.addEventListener('resize', this.designWindowChanged)
        this.repeatContainer.addEventListener('resize', this.repeatContainerWindowChanged)
        console.log('loadDesignComplete')
        defaultDesignWindow()
        initRepeatImageVars()
      }
    }
    /**
     * Handles +/- button click events for the controls
     * calls sliderChange()
     */
    function plusMinus (e) {
      switch (e.currentTarget.id) {
        case 'ZO':
          this.repZSl.value = this.repZSl.value - 1
          break
        case 'ZI':
          this.repZSl.value = this.repZSl.value + 1
          break
        case 'PW':
          this.repWSl.value = this.repWSl.value + 1
          break
        case 'MW':
          this.repWSl.value = this.repWSl.value - 1
          break
        case 'PH':
          this.repHSl.value = this.repHSl.value + 1
          break
        case 'MH':
          this.repHSl.value = this.repHSl.value - 1
          break
      }
      sliderChange()
    }
  /**
   * * Part of main loop!
   * calls setDrop()
   * calls displayRepeat()
   */
  class displayThumb {
    constructor () {
      this.defaultThumbWidth = 80
      this.defaultThumbHeight = 80
      this.aspectRatio = new Matrix()
      this.thumbBMD = new BitmapData(this.repW, this.repH, true, 0xFFFFFFFF)
      if (this.designW > this.designH) {
        this.thumbWidth = this.defaultThumbWidth
        this.thumbScale = this.defaultThumbWidth / this.designW
        this.thumbHeight = int(this.designH * this.thumbScale)
      } else {
        this.thumbHeight = this.defaultThumbHeight
        this.thumbScale = this.defaultThumbHeight / this.designH
        this.thumbWidth = int(this.designW * this.thumbScale)
      }
      this.aspectRatio.scale(this.thumbScale, this.thumbScale)
      this.thumbBMD.draw(this.designBitMapData, this.aspectRatio)
      this.thumbRepW = this.repW * this.thumbScale
      this.thumbRepH = this.repH * this.thumbScale
      this.thumbBMD.colorTransform(new Rectangle((this.thumbWidth / 2) - (this.thumbRepW / 2), (this.thumbHeight / 2) - (this.thumbRepH / 2), this.thumbRepW, this.thumbRepH), new ColorTransform(1, 1, 1, 1, this.hiloLightValue, this.hiloLightValue, this.hiloLightValue, 0))
      this.xRepStart = int(this.designW / 2) - int(this.repW / 2) // 0 Base
      this.yRepStart = int(this.designH / 2) - int(this.repH / 2) // 0 Base
      this.xRepRemainder = this.designW - (this.repW + this.xRepStart) // Absolute Size
      this.yRepRemainder = this.designH - (this.repH + this.yRepStart) // Absolute Size
      console.log('displayThumb')
      setDrop()
      displayRepeat()
    }
    }
    /**
     * * Other part of the main loop!
     * calls setDrop()
     * calls displayThumb()
     */
    function dropChanged () {
      setDrop()
      displayThumb()
    }
    /**
     * Is event argument even needed for these two local functions?
     * calls defaultDesignWindow()
     * @param Event I don't know
     */
    function designWindowChanged (Event) {
      console.log('designWindowChanged')
      defaultDesignWindow()
    }
    /**
     * calls displayRepeat()
     * @param Event I don't know
     */
    function repeatContainerWindowChanged (Event) {
      console.log('repeatContainerWindowChanged')
      displayRepeat()
    }
    function wRange (e) {
      switch (e.currentTarget.id) {
        case 'PW':
          this.repWSl.value = this.repWSl.value + 1
          break
        case 'MW':
          this.repWSl.value = this.repWSl.value - 1
          break
      }
      sliderChange()
    }
    function vRange (e) {
      switch (e.currentTarget.id) {
        case 'PH':
          this.repHSl.value = this.repHSl.value + 1
          break
        case 'MH':
          this.repHSl.value = this.repHSl.value - 1
          break
      }
      sliderChange()
    }
    function zRange (e) {
      switch (e.currentTarget.id) {
        case 'ZO':
          this.repZSl.value = this.repZSl.value - 1
          break
        case 'ZI':
          this.repZSl.value = this.repZSl.value + 1
          breakq
      }
      sliderChange()
    }
  }
}

