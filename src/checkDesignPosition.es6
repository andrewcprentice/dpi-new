class checkDesignPosition {
  constructor () {
    this.designView = document.getElementById('designView')
    if (this.designView.y > (this.designWindow.height / 2)) {
      this.designView.y = this.designWindow.height / 2
    }
    if ((this.designView.y + this.designView.height) < (this.designWindow.height / 2)) {
      this.designView.y = (this.designWindow.height / 2) - this.designView.height
    }
    if (this.designView.x > (this.designWindow.width / 2)) {
      this.designView.x = this.designWindow.width / 2
    }
    if ((this.designView.x + this.designView.width) < (this.designWindow.width / 2)) {
      this.designView.x = (this.designWindow.width / 2) - this.designView.width
    }
    console.log('checkDesignPosition')
  }
}
