import int from 'int'

import BitmapData from '../node_modules/openfl/lib/openfl/display/BitmapData'
import ColorTransform from '../node_modules/openfl/lib/openfl/geom/ColorTransform'
import Matrix from '../node_modules/openfl/lib/openfl/geom/Matrix'
import Rectangle from '../node_modules/openfl/lib/openfl/geom/Rectangle'

import displayRepeat from './src/displayRepeat.es6'
import setDrop from './src/setDrop.es6'

/**
 * * Part of main loop!
 * calls setDrop()
 * calls displayRepeat()
 */
class displayThumb {
  constructor () {
    this.defaultThumbWidth = 80
    this.defaultThumbHeight = 80
    this.aspectRatio = new Matrix()
    this.thumbBMD = new BitmapData(this.repW, this.repH, true, 0xFFFFFFFF)
    if (this.designW > this.designH) {
      this.thumbWidth = this.defaultThumbWidth
      this.thumbScale = this.defaultThumbWidth / this.designW
      this.thumbHeight = int(this.designH * this.thumbScale)
    } else {
      this.thumbHeight = this.defaultThumbHeight
      this.thumbScale = this.defaultThumbHeight / this.designH
      this.thumbWidth = int(this.designW * this.thumbScale)
    }
    this.aspectRatio.scale(this.thumbScale, this.thumbScale)
    this.thumbBMD.draw(this.designBitMapData, this.aspectRatio)
    this.thumbRepW = this.repW * this.thumbScale
    this.thumbRepH = this.repH * this.thumbScale
    this.thumbBMD.colorTransform(new Rectangle((this.thumbWidth / 2) - (this.thumbRepW / 2), (this.thumbHeight / 2) - (this.thumbRepH / 2), this.thumbRepW, this.thumbRepH), new ColorTransform(1, 1, 1, 1, this.hiloLightValue, this.hiloLightValue, this.hiloLightValue, 0))
    this.xRepStart = int(this.designW / 2) - int(this.repW / 2) // 0 Base
    this.yRepStart = int(this.designH / 2) - int(this.repH / 2) // 0 Base
    this.xRepRemainder = this.designW - (this.repW + this.xRepStart) // Absolute Size
    this.yRepRemainder = this.designH - (this.repH + this.yRepStart) // Absolute Size
    console.log('displayThumb')
    setDrop()
    displayRepeat()
  }
}
