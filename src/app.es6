import AppSprite from '/src/appsprite.es6'
import Stage from '../node_modules/openfl/lib/openfl/display/Stage'

const stage = new Stage(window.innerWidth, window.innerHeight - 700, 0xFFFFFF, AppSprite, {
  renderer: 'dom'
})
document.body.appendChild(stage.element)
