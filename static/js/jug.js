/**
 * Draws an image to a given canvas
 * 
 * @param {Image} image an image file
 * @param {Canvas} opt_cvs specify canvas
 * @param {Function} cb callback
 */
function imageToCanvas (image, opt_cvs, cb) {
  if (!cb) { cb = opt_cvs; opt_cvs = null }
  var cvs = opt_cvs || document.createElement('canvas')
  var ctx = cvs.getContext('2d')
  cvs.width = image.width
  cvs.height = image.height
  ctx.drawImage(image, 0, 0)
  cb(null, cvs)
}

/**
 * Loads image to an array buffer
 * @param {Image} image an image file
 * @param {Function} cb callback
 */
function imageToArrayBuffer (image, cb) {
  var reader = new FileReader()
  reader.onload = function () {
    cb(reader.error, reader.result)
  }
  imageToCanvas(image, function (err, cvs) {
    cvs.toBlob(reader.readAsArrayBuffer.bind(reader))
  })
}

/**
 * Loads a generic file to an array buffer
 * @param {String} file a file path name
 * @param {Function} cb callback
 */
function fileToArrayBuffer (file, cb) {
  var reader = new FileReader()
  reader.onload = function () {
    cb(reader.error, reader.result)
  }
  reader.readAsArrayBuffer(file)
}

/**
 * Writes image data to an image object
 * @param {ImageData} imageData processed image data
 * @param {Image} opt_image Image to fill with data
 * @param {Function} cb callback
 */
function imageDataToImage (imageData, opt_image, cb) {
  if (!cb) { cb = opt_image; opt_image = null }
  var img = opt_image || document.createElement('img')
  var cvs = document.createElement('canvas')
  var ctx = cvs.getContext('2d')
  cvs.width = imageData.width
  cvs.height = imageData.height
  ctx.putImageData(imageData, 0, 0)
  canvasToImage(cvs, img, cb)
}

/**
 * Loads an image file to an image object
 * @param {FileReader} file image file to load
 * @param {Image} opt_image image object
 * @param {Function} cb callback
 */
function fileToImage (file, opt_image, cb) {
  if (!cb) { cb = opt_image; opt_image = null }
  var img = opt_image || document.createElement('img')
  var url = URL.createObjectURL(file)
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  img.src = url
}

/**
 * Takes a snapshot of the canvas and writes it to an image
 * @param {Canvas} canvas the canvas to read
 * @param {Image} opt_image image to write canvas data to
 * @param {Function} cb callback
 */
function canvasToImage (canvas, opt_image, cb) {
  if (!cb) { cb = opt_image; opt_image = null }
  var url
  var img = opt_image || document.createElement('img')
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  canvas.toBlob(function (blob) {
    url = URL.createObjectURL(blob)
    img.src = url
  })
}

/**
 * 
 * @param {Blob} blob binary blob data
 * @param {Image} opt_image image object to fill with data
 * @param {Function} cb callback
 */
function blobToImage (blob, opt_image, cb) {
  if (!cb) { cb = opt_image; opt_image = null }
  var img = opt_image || document.createElement('img')
  var url = URL.createObjectURL(blob)
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  img.src = url
}

/**
 * Draws ImageData array to Canvas
 * @param {ImageData} pixels ImageData to send
 * @param {Canvas} opt_canvas Where to send it
 * @param {Function} cb callback
 */
function pixelsToCanvas (pixels, opt_canvas, cb) {
  if (!cb) { cb = opt_canvas; opt_canvas = null }
  var cvs = opt_canvas || document.createElement('canvas')
  var width = pixels.width || cvs.width
  var height = pixels.height || cvs.height
  cvs.width = width
  cvs.height = height
  var ctx = cvs.getContext('2d')
  var imgdata = ctx.createImageData(width, height)
  for (var i = 0; i < pixels.length; i++) {
    imgdata.data[i] = pixels[i]
  }
  ctx.putImageData(imgdata, 0, 0)
  cb(null, cvs)
}

exports.imageToCanvas = imageToCanvas
exports.imageToArrayBuffer = imageToArrayBuffer
exports.imageDataToImage = imageDataToImage
exports.fileToArrayBuffer = fileToArrayBuffer
exports.fileToImage = fileToImage
exports.canvasToImage = canvasToImage
exports.blobToImage = blobToImage
exports.pixelsToCanvas = pixelsToCanvas