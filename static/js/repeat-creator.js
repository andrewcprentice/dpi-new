'use strict'
import jug from './jug'
// * ------HELPER FUNCTIONS---------
/**
 * * Image to Canvas
 * @param {Image} image the source
 * @param {Canvas} optCvs the canvas for the work space
 * @param {Function} imgcb imgcb
 */
function imageToCanvas (image, optCvs, cb) {
  if (!cb) {
    cb = optCvs
    optCvs = null
  }
  var cvs = optCvs || document.createElement('canvas')
  var ctx = cvs.getContext('2d')
  cvs.width = image.width
  cvs.height = image.height
  ctx.drawImage(image, 0, 0)
  cb(null, cvs)
}
/**
 * * File to Image
 * @param {File} file the loaded image file
 * @param {Image} optImage Image element to attach the loaded file to src attr
 * @param {Function} cb imgcb
 */
function fileToImage (file, optImage, cb) {
  if (!cb) {
    cb = optImage
    optImage = null
  }
  var img = optImage || document.createElement('img')
  var url = URL.createObjectURL(file)
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  img.src = url
}
/**
 * Canvas to image
 * @param {Canvas} canvas The canvas to convert
 * @param {Image} optImage The destination image element
 * @param {Function} cb imgcb
 */
function canvasToImage (canvas, optImage, cb) {
  if (!cb) {
    cb = optImage
    optImage = null
  }
  var url
  var img = optImage || document.createElement('img')
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  canvas.toBlob(function (blob) {
    url = URL.createObjectURL(blob)
    img.src = url
  })
}
// * ------VARIABLE DECLARATIONS---------
/**
 * Basic image cb function
 * @param {Error} err 
 * @param {Image} img 
 */
var imgcb = function (err, img) {
  if (err) {
    return err
  } else {
    return img
  }
}
var jimg = $('#loaded')
var simg = $('#saved')
var eimg = $('#exported')
var canvasElement = document.getElementById('repeat-creator-canvas')
var ctx = canvasElement.getContext('2d')
var $horizontalSlider = $('#horizontal-adjust')
var $verticalSlider = $('#vertical-adjust')
var $document = $(document)
var maskColor = ''
var imgData = {}
var dropDirection = 'None'
var dropCount = 0
var sections = ['TL', 'TC', 'TR', 'ML', 'MC', 'MR', 'BL', 'BC', 'BR']
var virtualCanvas = document.createElement('canvas')
var maskCanvas = document.createElement('canvas')
var virtualCtx = virtualCanvas.getContext('2d')
var maskCtx = maskCanvas.getContext('2d')
var input = document.querySelector('input[type="file"]')
/**
 * Run when canvas is resized, scales image,
 * calls initiateSliderValues()
 */
var resizeCanvas = function () {
  if (imgData.width > imgData.height) {
    imgData.scaled_width = (window.innerWidth * 0.75)
    imgData.scale = imgData.scaled_width / imgData.width
    imgData.scaled_height = imgData.height * imgData.scale
    if (imgData.scaled_height > window.innerHeight) {
      imgData.scaled_height = window.innerHeight * 0.8 - $('#repeat-creator-toolbar').innerHeight()
      imgData.scale = imgData.scaled_height / imgData.height
      imgData.scaled_width = imgData.width * imgData.scale
    }
  } else {
    if (window.innerWidth > window.innerHeight) {
      imgData.scaled_height = (window.innerHeight * 0.85) - $('#repeat-creator-toolbar').innerHeight()
      imgData.scale = imgData.scaled_height / imgData.height
      imgData.scaled_width = imgData.width * imgData.scale
    } else {
      imgData.scaled_width = (window.innerWidth * 0.75)
      imgData.scale = imgData.scaled_width / imgData.width
      imgData.scaled_height = imgData.height * imgData.scale
    }
  }
  $(canvasElement).width(imgData.scaled_width + 60)
  $(canvasElement).height(imgData.scaled_height + 60)
  initiateSliderValues()
  canvasElement.width = imgData.scaled_width + 60
  canvasElement.height = imgData.scaled_height + 60
}

/**
* Need to modify this to load from a different source, namely file input
* calls resetMaskColor(), resizeCanvas(), virtualCtx.drawImage(), maskCtx.drawImage(),
* drawToCanvas(), repositionSliders()
* @param {ImgElement} jimg The <img> element to load
*/
var loadImage = function (jimg) {
  jimg.crossOrigin = 'Anonymous'
  resetMaskColor()
  jimg.onload = function () {
    imgData.width = imgData.$selected.data('width')
    imgData.height = imgData.$selected.data('height')
    resizeCanvas()
    virtualCanvas.width = imgData.$selected.data('width')
    virtualCanvas.height = imgData.$selected.data('height')
    maskCanvas.width = imgData.$selected.data('width')
    maskCanvas.height = imgData.$selected.data('height')
    virtualCtx.drawImage(this, 0, 0, imgData.$selected.data('width'), imgData.$selected.data('height'))
    maskCtx.drawImage(this, 0, 0, imgData.$selected.data('width'), imgData.$selected.data('height'))
    drawToCanvas(ctx, canvasElement, true)
    repositionSliders()
  }
  jimg.src = imgData.activeImage
}
/**
* The main draw function
* calls clearCanvas(), context.drawImage
* @param {Canvas.context} context The canvas context to draw to
* @param {Canvas} canvas The canvas element
* @param {Boolean} clear Whether to clear canvas before draw
* @param {Boolean} scaled Whether to scale or not
* @param {Function} callback Tie this up nice and neat
*/
var drawToCanvas = function (context, canvas, clear, scaled, callback) {
  var top = 0
  var left = 0
  var imgheight = imgData.scaled_height
  var imgwidth = imgData.scaled_width
  var offsetX = (imgData.scaled_width - ($horizontalSlider.val()) * imgData.scale) / 2
  var offsetY = (imgData.scaled_height - ($verticalSlider.val()) * imgData.scale) / 2
  if (!scaled && scaled !== undefined) {
    imgheight = imgData.height
    imgwidth = imgData.width
    offsetX = (imgwidth - $horizontalSlider.val()) / 2
    offsetY = (imgheight - $verticalSlider.val()) / 2
  }
  if (clear) {
    clearCanvas(context, canvas.width, canvas.height)
  }
  var doVertDrop = false
  var doHorDrop = false
  // var drawThis = false
  for (var section in sections) {
    switch (sections[section]) {
      case 'TL':
        top = ((0 - imgheight) + (offsetY * 2)) + 30
        left = ((0 - imgwidth) + (offsetX * 2)) + 30
        doVertDrop = true
        doHorDrop = true
        // drawThis = false
        break
      case 'TC':
        top = ((0 - imgheight) + (offsetY * 2)) + 30
        left = 30
        doVertDrop = false
        doHorDrop = true
        // drawThis = false
        break
      case 'TR':
        top = ((0 - imgheight) + (offsetY * 2)) + 30
        left = (imgwidth - (offsetX * 2)) + 30
        doVertDrop = true
        doHorDrop = true
        // drawThis = false
        break
      case 'ML':
        top = 30
        left = ((0 - imgwidth) + (offsetX * 2)) + 30
        doVertDrop = true
        doHorDrop = false
        // drawThis = false
        break
      case 'MC':
        top = 30
        left = 30
        doVertDrop = false
        doHorDrop = false
        // drawThis = true
        break
      case 'MR':
        top = 30
        left = (imgwidth - (offsetX * 2)) + 30
        doVertDrop = true
        doHorDrop = false
        // drawThis = false
        break
      case 'BL':
        top = (imgheight - (offsetY * 2)) + 30
        left = (0 - imgwidth) + (offsetX * 2) + 30
        doVertDrop = true
        doHorDrop = true
        // drawThis = true
        break
      case 'BC':
        top = (imgheight - (offsetY * 2)) + 30
        left = 30
        doVertDrop = false
        doHorDrop = true
        // drawThis = true
        break
      case 'BR':
        top = (imgheight - (offsetY * 2)) + 30
        left = imgwidth - (offsetX * 2) + 30
        doVertDrop = true
        doHorDrop = true
        // drawThis = true
        break
    }
    if (dropDirection === 'vertical' && doVertDrop) {
      top += imgheight / dropCount
    }
    if (dropDirection === 'horizontal' && doHorDrop) {
      left += imgwidth / dropCount
    }
    context.drawImage(virtualCanvas, left, top, imgwidth, imgheight)
  }
  if (typeof callback === 'function') {
    callback()
  }
}
/**
* Masks canvas. Calls maskCtx.getImageData(),
* virtualCtx.putImageData(), drawToCanvas()
* @param {Array} mask pixel vector
*/
var drawMaskCanvas = function (mask) {
  var transparentData = maskCtx.getImageData(0, 0, imgData.$selected.data('width'), imgData.$selected.data('height'))
  for (var i = 0; i < transparentData.data.length; i += 4) {
    if (transparentData.data[i] === parseInt(mask[0]) &&
      transparentData.data[i + 1] === parseInt(mask[1]) &&
      transparentData.data[i + 2] === parseInt(mask[2]) &&
      transparentData.data[i + 3] === parseInt(mask[3])
    ) {
      transparentData.data[i + 3] = 0
    }
  }
  virtualCtx.putImageData(transparentData, 0, 0)
  drawToCanvas(ctx, canvasElement, true)
}
var clearCanvas = function (context, width, height) {
  context.clearRect(0, 0, width, height)
}
var repositionSliders = function () {
  $horizontalSlider.parent().css('width', function () {
    return $(canvasElement).width() / 2
  })
  $horizontalSlider.parent().css('margin-left', function () {
    return -($(this).width() / 2)
  })
  $verticalSlider.parent().css('width', function () {
    return $(canvasElement).height() / 2
  })
  $verticalSlider.parent().css('left', function () {
    var left = (window.innerWidth / 2) - (imgData.scaled_width / 2) - ($(this).width() / 2) - 55
    return left
  })
}
/**
* calls drawToCanvas()
* @returns [Canvas] tempCanvas
*/
var getRepeatCanvas = function () {
  var width = $horizontalSlider.val()
  var height = $verticalSlider.val()
  var offsetX = (imgData.width - $horizontalSlider.val()) / 2
  var offsetY = (imgData.height - $verticalSlider.val()) / 2
  var tempCanvas = document.createElement('canvas')
  var imageCanvas = document.createElement('canvas')
  var tempCtx = tempCanvas.getContext('2d')
  var imgCtx = imageCanvas.getContext('2d')
  var r = maskColor[0]
  var g = maskColor[1]
  var b = maskColor[2]
  var a = maskColor[3]
  var rgba = 'rgba(' + r + ', ' + g + ',' + b + ',' + a + ')'
  tempCanvas.width = width
  tempCanvas.height = height
  imageCanvas.width = imgData.width
  imageCanvas.height = imgData.height
  imgCtx.fillStyle = rgba
  imgCtx.fillRect(0, 0, imgData.width, imgData.height)
  drawToCanvas(imageCanvas.getContext('2d'), imageCanvas, false, false)
  var imageData = imgCtx.getImageData(offsetX, offsetY, width, height)
  tempCtx.putImageData(imageData, 0, 0)
  return tempCanvas
}
var initiateSliderValues = function () {
  var minVert = Math.round(imgData.height / 2)
  var minHor = Math.round(imgData.width / 2)
  var maxVert = Math.round(imgData.height - 30)
  var maxHor = Math.round(imgData.width - 30)
  $horizontalSlider.attr('min', minHor).attr('max', maxHor).val(maxHor)
  $verticalSlider.attr('min', minVert).attr('max', maxVert).val(maxVert)
}
var resetMaskColor = function () {
  $('#mask-color').removeAttr('style')
  $horizontalSlider.parent().addClass('hidden')
  $verticalSlider.parent().addClass('hidden')
}
/**
* * Calls resizeCanvas(), drawToCanvas(), repositionSliders()
*/
var redrawCanvas = function () {
  resizeCanvas()
  drawToCanvas(ctx, canvasElement, true)
  repositionSliders()
}
/**
 * Should display mask color text
 */
var selectColor = false
$(document).on('ready', function (e) {
  $('body').on('click', function (e) {
    if (e.target.id === 'repeat-creator-canvas' && selectColor) {
      maskColor = e.target.getContext('2d').getImageData(e.offsetX, e.offsetY, 1, 1).data
      var rgba = 'rgba(' + maskColor.join(',') + ')'
      if (rgba === 'rgba(0,0,0,0)') {
        rgba = $('#repeat-creator-canvas').css('background-color').replace('rgb(', 'rgba(').replace(')', ', 255)')
        maskColor = rgba.replace('rgba(', '').replace(')', '').split(', ')
      }
      $('#mask-color').css('background-color', rgba)
      $('#repeat-creator canvas').css('background-color', rgba)
      $horizontalSlider.parent().removeClass('hidden')
      $verticalSlider.parent().removeClass('hidden')
      selectColor = false
      drawMaskCanvas(maskColor)
    }
  })
  $('.user-images').click(function (e) {
    var target = $(e.target)
    if (target.is('input[type="radio"]')) {
      imgData.activeImage = $(target).data('thumb')
      imgData.$selected = $(target)
      loadImage()
    }
  })
  $('.repeat-adjust').click(function (e) {
    var target = $(e.target)
    if (target.is('button')) {
      e.preventDefault()
      if (target.hasClass('increment')) {
        target.siblings('input').val(function (index, value) {
          return parseInt(value) + parseInt($(this).attr('step'))
        })
        $horizontalSlider.trigger('change')
      } else if (target.hasClass('decrement')) {
        target.siblings('input').val(function (index, value) {
          return parseInt(value) - parseInt($(this).attr('step'))
        })
        $horizontalSlider.trigger('change')
      }
    }
  })
  $('#repeat-drop').on('change', function (e) {
    var $selectedOption = $(this).find('option:selected')
    dropDirection = $selectedOption.data('offset')
    dropCount = $selectedOption.data('value')
    drawToCanvas(ctx, canvasElement, true)
  })
  $horizontalSlider.on('change input', function (e) {
    drawToCanvas(ctx, canvasElement, true)
  })
  $verticalSlider.on('change input', function (e) {
    drawToCanvas(ctx, canvasElement, true)
  })
  $(window).on('resize', function (e) {
    var resizeTimer = setTimeout(redrawCanvas, 100)
    clearTimeout(resizeTimer)
  })
  $("#vda").click(function(){
    $("#advanced").slideReveal("hide");
  });
})

input.addEventListener('change', function (e) {
  e.stopPropagation()
  var file = e.target.files[0]
  if (!file) {
    throw new Error('No file or invalid file was selected')
  }
  fileToImage(file, jimg, function (err, img) {
    if (!err) {
      jug.imageToCanvas(img, canvasElement, imgcb)
      imgData.activeImage = img
      imgData.activeImage.width = 100
      imgData.activeImage.height = 100
      imgData.$selected = img
      loadImage()
      drawPattern(img)
    }
  }, false)
})

var drawPattern = function (img) {
  canvasElement.height = $document.height()
  canvasElement.width = $document.width()
  ctx.clearRect(0, 0, canvasElement.width, canvasElement.height)
  ctx.fillStyle = ctx.createPattern(img, 'repeat')
  ctx.beginPath()
  ctx.rect(0, 0, canvasElement.width, canvasElement.height)
  ctx.fill()
}
